// obj file loader
// loads an obj file and makes 3 unindexed arrays
// on a pmesh structure

#include <kos.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "data_estructures.h"

#define BUFFER_SIZE 512
// buffer for use during parsing

// function used to asses the number of vertices, normals and UV coordinates

int getNumberOfToken(FILE *fp, char *token) {
	int fp_status = 0;
	char lineHeader[BUFFER_SIZE];
	int ret = 0;

	while(1) {
		fp_status = fscanf(fp, "%s", lineHeader);
		if(fp_status == EOF) {
			break;
		}

		if(strcmp(lineHeader, token) == 0) {
			ret++;
		}
	}
	rewind(fp);
	return ret;
}

// Function to create the arrays to hold the vertices loaded from the file

vec3 *genVec3Array(int vertices_count) {
	vec3 *ret_ptr = (vec3 *)malloc(sizeof(vec3) * vertices_count);
	return ret_ptr;
}

vec2 *genVec2Array(int vertices_count) {
	vec2 *ret_ptr = (vec2 *)malloc(sizeof(vec2) * vertices_count);
	return ret_ptr;
}

int *genIntArray(int size) {
	int *ret_ptr = (int *)malloc(sizeof(int) * size);
	return ret_ptr;
}

// loads the vertces from the FILE onto RAM.

vec3 *loadTkVec3(FILE *fp, int vertices_count, char *token) {
	vec3 *ret_ptr = NULL;
	ret_ptr = genVec3Array(vertices_count);  // allocate memory

	char lineHeader[BUFFER_SIZE];
	lineHeader[BUFFER_SIZE - 1] = '\0';

	int fread_stat = 0;
	int curr_v = 0;
	while(1) {
		fread_stat = fscanf(fp, "%s", lineHeader);
		if(fread_stat == EOF) {
			break;
		}

		if(strcmp(lineHeader, token) == 0) {
			fscanf(fp, "%f %f %f\n", &ret_ptr[curr_v].x, &ret_ptr[curr_v].y, &ret_ptr[curr_v].z);
			// printf("%f %f %f \t %d\n", ret_ptr[curr_v].x, ret_ptr[curr_v].y, ret_ptr[curr_v].z, curr_v );
			curr_v++;
		}
	}

	rewind(fp);
	return ret_ptr;
}

vec2 *loadTkVec2(FILE *fp, int uv_coord_cnt, char *token) {
	vec2 *ret_ptr = genVec2Array(uv_coord_cnt);

	char lineHeader[BUFFER_SIZE];
	lineHeader[BUFFER_SIZE - 1] = '\0';

	int fread_stat = 0;
	int curr_v = 0;
	while(1) {
		fread_stat = fscanf(fp, "%s", lineHeader);
		if(fread_stat == EOF) {
			break;
		}

		if(strcmp(lineHeader, token) == 0) {
			fscanf(fp, "%f %f\n", &ret_ptr[curr_v].u, &ret_ptr[curr_v].v);
			// printf("%f %f\t %d\n", ret_ptr[curr_v].u, ret_ptr[curr_v].v, curr_v );
			curr_v++;
		}
	}

	rewind(fp);
	return ret_ptr;
}

vec3 *loadVertices(FILE *fp) {
	int vertices = getNumberOfToken(fp, "v");
	vec3 *ret_array = loadTkVec3(fp, vertices, "v");

	return ret_array;
}


float getVecLength(vec3 a)
{
	return sqrtf( (a.x * a.x) + (a.y * a.y) + (a.z * a.z));
}

void vec3Normalize(vec3 *v, int n)
{
	int iter = 0 ;
	float curr_L = 0.0 ;

	for(iter = 0 ; iter < n ; iter++)
		{
			
			//printf("Normal : %f, %f, %f \n",v[iter].x ,v[iter].y, v[iter].z );

			curr_L = getVecLength(v[iter]);
			v[iter].x = v[iter].x / curr_L ;
			v[iter].y = v[iter].y / curr_L ;
			v[iter].z = v[iter].z / curr_L ;
			
		}

}





vec3 *loadNormals(FILE *fp) {
	int normals = getNumberOfToken(fp, "vn");
	vec3 *ret_array = loadTkVec3(fp, normals, "vn");
	vec3Normalize(ret_array, normals);

	return ret_array;
}

vec2 *loadUV(FILE *fp) {
	int uv = getNumberOfToken(fp, "vt");
	vec2 *ret_array = loadTkVec2(fp, uv, "vt");

	return ret_array;
}


// once we have all the vertex, normal, uv data from the file into RAM we need to read the indices and generate the unindexed arrays
// local struct for indices data. its meant for local use only

typedef struct sIndices {
	int v;
	int vt;
	int vn;
} Indices;

typedef Indices *pIndices;

pIndices loadIndices(FILE *fp) {
	int faceNumber = getNumberOfToken(fp, "f");
	// since we deal only with triangles, each face has 3 vertices
	int verticesCount = faceNumber * 3;

	pIndices ret_indices = (pIndices)malloc(sizeof(Indices) * verticesCount);

	char lineHeader[BUFFER_SIZE];
	lineHeader[BUFFER_SIZE - 1] = '\0';

	int fread_stat = 0;
	int curr_v = 0;
	int matches = 0;

	while(1) {
		fread_stat = fscanf(fp, "%s", lineHeader);
		if(fread_stat == EOF) {
			break;
		}

		if(strcmp(lineHeader, "f") == 0) {
			matches = fscanf(fp, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &ret_indices[curr_v].v, &ret_indices[curr_v].vt, &ret_indices[curr_v].vn, &ret_indices[curr_v + 1].v, &ret_indices[curr_v + 1].vt, &ret_indices[curr_v + 1].vn, &ret_indices[curr_v + 2].v, &ret_indices[curr_v + 2].vt, &ret_indices[curr_v + 2].vn);

			if(matches != 9) {
				printf("is the model triangulated ?? \n\n");
				return NULL;
			}
			--ret_indices[curr_v].v;
			--ret_indices[curr_v].vt;
			--ret_indices[curr_v].vn;

			--ret_indices[curr_v + 1].v;
			--ret_indices[curr_v + 1].vt;
			--ret_indices[curr_v + 1].vn;

			--ret_indices[curr_v + 2].v;
			--ret_indices[curr_v + 2].vt;
			--ret_indices[curr_v + 2].vn;

			// printf("%d/%d/%d %d/%d/%d %d/%d/%d\n", ret_indices[curr_v].v, ret_indices[curr_v].vt, ret_indices[curr_v].vn,
			//										ret_indices[curr_v+1].v, ret_indices[curr_v+1].vt, ret_indices[curr_v+1].vn,
			//										ret_indices[curr_v+2].v, ret_indices[curr_v+2].vt, ret_indices[curr_v+2].vn);

			curr_v = curr_v + 3;
		}
	}
	rewind(fp);
	return ret_indices;
}

// now that we have our indices, we may begin to un index the data
// type defines the type of array : 0 to generate a vertex array, 1 to generate a normal array
vec3 *genVec3ArrayFromIndices(vec3 *input_array, pIndices i, int face_number, int type) {

	int verticesNumber = face_number * 3;
	vec3 *ret_ptr = (vec3 *)malloc(sizeof(vec3) * verticesNumber);

	int array_iter = 0;
	int curr_indice = 0;

	// we iterate over the indices list
	if(type == 0) {
		while(array_iter < face_number * 3) {

			// fetch the indices
			curr_indice = i[array_iter].v;
			// 1st vertice
			ret_ptr[array_iter].x = input_array[curr_indice].x;
			ret_ptr[array_iter].y = input_array[curr_indice].y;
			ret_ptr[array_iter].z = input_array[curr_indice].z;
			

/*
			 printf("curr_indice = %d \t array_iter = %d \t x: %f y: %f z : %f \n", curr_indice, array_iter,
									ret_ptr[array_iter].x,
									ret_ptr[array_iter].y,
									ret_ptr[array_iter].z );

			*/
			// 2nd vertice
			++array_iter;
			curr_indice = i[array_iter].v;

			ret_ptr[array_iter].x = input_array[curr_indice].x;
			ret_ptr[array_iter].y = input_array[curr_indice].y;
			ret_ptr[array_iter].z = input_array[curr_indice].z;
/*
			 printf("curr_indice = %d \t array_iter = %d \t x: %f y: %f z : %f \n", curr_indice, array_iter,
									ret_ptr[array_iter].x,
									ret_ptr[array_iter].y,
									ret_ptr[array_iter].z );
*/
			// 3rd vertice

			++array_iter;
			curr_indice = i[array_iter].v;

			ret_ptr[array_iter].x = input_array[curr_indice].x;
			ret_ptr[array_iter].y = input_array[curr_indice].y;
			ret_ptr[array_iter].z = input_array[curr_indice].z;
/*
				printf("curr_indice = %d \t array_iter = %d \t x: %f y: %f z : %f \n", curr_indice, array_iter,
										ret_ptr[array_iter].x,
										ret_ptr[array_iter].y,
										ret_ptr[array_iter].z );
*/
			// increase indice for the next loop.
			++array_iter;
		}
	} else {
		while(array_iter < face_number * 3) {

			// fetch the indices
			curr_indice = i[array_iter].vn;
			// 1st vertice
			ret_ptr[array_iter].x = input_array[curr_indice].x;
			ret_ptr[array_iter].y = input_array[curr_indice].y;
			ret_ptr[array_iter].z = input_array[curr_indice].z;
/*
			printf("Normal array : curr_indice = %d \t array_iter = %d \t x: %f y: %f z : %f \n", curr_indice, array_iter,
									ret_ptr[array_iter].x,
									ret_ptr[array_iter].y,
									ret_ptr[array_iter].z );

*/
			// 2nd vertice
			array_iter++;
			curr_indice = i[array_iter].vn;

			ret_ptr[array_iter].x = input_array[curr_indice].x;
			ret_ptr[array_iter].y = input_array[curr_indice].y;
			ret_ptr[array_iter].z = input_array[curr_indice].z;
/*

			printf("Normal array : curr_indice = %d \t array_iter = %d \t x: %f y: %f z : %f \n", curr_indice, array_iter,
									ret_ptr[array_iter].x,
									ret_ptr[array_iter].y,
									ret_ptr[array_iter].z );
*/
			// 3rd vertice

			array_iter++;
			curr_indice = i[array_iter].vn;

			ret_ptr[array_iter].x = input_array[curr_indice].x;
			ret_ptr[array_iter].y = input_array[curr_indice].y;
			ret_ptr[array_iter].z = input_array[curr_indice].z;

/*
			printf("Normal array : curr_indice = %d \t array_iter = %d \t x: %f y: %f z : %f \n", curr_indice, array_iter,
									ret_ptr[array_iter].x,
									ret_ptr[array_iter].y,
									ret_ptr[array_iter].z );

*/
			// increase indice for the next loop.
			array_iter++;
		}
	}

	return ret_ptr;
}


// in order to debug the lightning issues I will compute new normals per triagle.
// all vertices of a given triagle will have the same normal

vec3 calcNormal(vec3 a, vec3 b, vec3 c)
{
	//void vec3Normalize(vec3 *v, int n)
	vec3 ret;
	vec3 ab;
	vec3 ac;

	float curr_L ;

	ab.x = a.x - b.x;
	ab.y = a.y - b.y;
	ab.z = a.z - b.z;

	ac.x = a.x - c.x;
	ac.y = a.y - c.y;
	ac.z = a.z - c.z;

	// cross product

	ret.x = (ab.y * ac.z) - (ac.y * ab.z);
	ret.y = (ac.x * ab.z) - (ab.x * ac.z);
	ret.z = (ab.x * ac.y) - (ac.x * ab.y);

	//normalize

	curr_L = getVecLength(ret);
	ret.x = ret.x / curr_L ;
	ret.y = ret.y / curr_L ;
	ret.z = ret.z / curr_L ;
	
	return ret ;
}

vec3 *genVec3ArrayNormals(vec3 *vertex_data, int face_number)
{
	int iter = 0 ;
	int verticesNumber = face_number * 3;
	vec3 *ret_ptr = (vec3 *)malloc(sizeof(vec3) * verticesNumber);
	vec3 currNormal;

	while(iter < face_number)
	{
		currNormal = calcNormal(vertex_data[iter],vertex_data[iter+1], vertex_data[iter+2]);
		ret_ptr[iter] = currNormal ;
		ret_ptr[iter+1] = currNormal ;
		ret_ptr[iter+2] = currNormal ;

		iter = iter + 3 ; 
	}

	return ret_ptr ;
}

vec2 *genVec2ArrayFromIndices(vec2 *input_array, pIndices i, int face_number) {

	int verticesNumber = face_number * 3;
	vec2 *ret_ptr = (vec2 *)malloc(sizeof(vec2) * verticesNumber);

	int array_iter = 0;
	int curr_indice = 0;

	// we iterate over the indices list
	while(array_iter < face_number * 3) {

		// fetch the indices
		curr_indice = i[array_iter].vt;
		// 1st uv coord
		ret_ptr[array_iter].u = input_array[curr_indice].u;
		ret_ptr[array_iter].v = input_array[curr_indice].v;

		// printf("curr_indice = %d \t array_iter = %d \t u: %f v: %f\n", curr_indice, array_iter,
		//						ret_ptr[array_iter].u,
		//						ret_ptr[array_iter].v );

		// 2nd vertice
		array_iter++;
		curr_indice = i[array_iter].vt;

		ret_ptr[array_iter].u = input_array[curr_indice].u;
		ret_ptr[array_iter].v = input_array[curr_indice].v;

		// printf("curr_indice = %d \t array_iter = %d \t u: %f v: %f\n", curr_indice, array_iter,
		//						ret_ptr[array_iter].u,
		//						ret_ptr[array_iter].v );
		// 3rd vertice

		array_iter++;
		curr_indice = i[array_iter].vt;

		ret_ptr[array_iter].u = input_array[curr_indice].u;
		ret_ptr[array_iter].v = input_array[curr_indice].v;

		//	printf("curr_indice = %d \t array_iter = %d \t u: %f v: %f\n", curr_indice, array_iter,
		//							ret_ptr[array_iter].u,
		//							ret_ptr[array_iter].v );

		// increase indice for the next loop.
		array_iter++;
	}

	return ret_ptr;
}
// void main()


void display_pmesh_data(pmesh p)
{
	int iter = 0;

	printf("\n DEBUG :\t mesh : %s \t vertex_count : %d \n", p->name, p->vertex_count);

	float *v_data = p->vertex_data;
	float *uv_data = p->uv_data;
	float *n_data = p->vertex_normal;


	while (iter < p->vertex_count)
	{
		

		printf("MODEL VERTICES : \t %f %f %f\n",v_data[iter], v_data[iter+1], v_data[iter+2]);
		//printf("MODEL UV : \t %f %f\n", uv_data[iter].u, uv_data[iter].v);
		printf("MODEL NORMALS : \t %f %f %f\n", n_data[iter], n_data[iter+1], n_data[iter+2]);
		iter = iter + 3;
	}

}

pmesh obj_loader(char *filename) {
	FILE *fp;
	fp = fopen(filename, "r");

	int faces = getNumberOfToken(fp, "f");
	vec3 *teste = loadVertices(fp);
	vec3 *testen = loadNormals(fp);
	vec2 *testeuv = loadUV(fp);

	pIndices tind = loadIndices(fp);

	// vec3 * unindexed_a = genVec3ArrayFromIndices(teste, tind , faces ,0);

	pmesh ret = (pmesh)malloc(sizeof(mesh));
	ret->vertex_data = (float *)genVec3ArrayFromIndices((vec3 *)teste, tind, faces, 0);
	ret->vertex_count = faces * 3;
	ret->uv_data = (float *)genVec2ArrayFromIndices((vec2 *)testeuv, tind, faces);

	ret->vertex_normal = (float *)genVec3ArrayFromIndices((vec3 *)testen, tind, faces, 1);
	

	//ret->vertex_normal = (float *) genVec3ArrayNormals(ret->vertex_data,faces);

	// free all the memory on the unused arrays
	free(teste);
	free(testen);
	free(testeuv);

	// printf("Number of vertices = %d \t Number of Normals = %d \t TexCoord = %d \t Number of Faces = %d \t \n", vertices, vertex_normals, texture_cood, faces);
	//display_pmesh_data(ret);
	//

	fclose(fp);
	return ret;
}