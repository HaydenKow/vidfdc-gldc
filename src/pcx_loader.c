#include <kos.h>
#include "gl_headers.h"
#include <pcx/pcx.h>
#include <dc/pvr.h>

int loadtxr(const char *fn, GLuint *txr, unsigned int *w, unsigned int *h) {
	kos_img_t imgaddr;

	if(pcx_to_img(fn, &imgaddr) < 0) {
		printf("can't load %s\n", fn);
		return 1;
	}

	printf("Loading %lux%lu PCX `%s'..\n", imgaddr.w, imgaddr.h, fn);

	*w = imgaddr.w;
	*h = imgaddr.h;

	glGenTextures(1, txr);
	glBindTexture(GL_TEXTURE_2D, *txr);

	if(imgaddr.fmt == KOS_IMG_FMT_RGB565) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imgaddr.w, imgaddr.h, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, imgaddr.data);
	} else {
		puts("Unhandled texture format.");
		return 1;
	}

	return 0;
}
