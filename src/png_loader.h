#ifndef PNGLOADER_H
#define PNGLOADER_H

#include "gl_headers.h"

int loadpng(const char *fn, GLuint *txr, int *w, int *h);

#endif
