#ifndef VIDFDC_MATRIX_H
#define VIDFDC_MATRIX_H

// Perspective matrix with far plane at infinity. Improves precision.
// Refer to the paper "Tightening the Precision of Perspective Rendering".
void setInfinitePerspectiveMat(float *m, float aspect, float fovDegrees, float const near);

#endif
