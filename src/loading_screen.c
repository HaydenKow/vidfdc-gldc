
#include <kos.h>
#include "gl_headers.h"

#include "data_estructures.h"
#include "dc_render.h"
// assumes that the screen is at WIDTH x HEIGHT

// global vars
float const width = 640;
float const height = 480;

typedef struct {
	float r, g, b;
} Color;

size_t const phases = 3;
size_t phase = 0;
static Color const colors[3] = { { 1, 0, 0 },   // nothing is loaded
	                             { 1, 1, 0 },   // 50%
	                             { 0, 0.5, 1 }  // 100%
};

float const lerpf(float a, float b, float alpha) { return a + alpha * (b - a); }

void lerpColor(Color *dest, Color const *from, Color const *to, float alpha) {
	dest->r = lerpf(from->r, to->r, alpha);
	dest->g = lerpf(from->g, to->g, alpha);
	dest->b = lerpf(from->b, to->b, alpha);
}

void drawLoadingBar(pDemoAssetList de) {
	LoadingStatus const *loadingStatus = &de->loading_status;
	float const progress = (float)loadingStatus->loaded_assets / loadingStatus->total_assets;

	// Phase 0: 0-50%, phase 1: 50-100%
	size_t const phase = progress > 0.5f;
	size_t const nextPhase = phase + 1;

	Color color;
	lerpColor(&color, &colors[phase], &colors[nextPhase], 2.f * (progress - 0.5f * phase));

	float const x = 10;
	float const y = 10;
	float const w = width - 2.f * x;
	float const h = height / 8.f;

	glColor3f(0.2f, 0.2f, 0.2f);
	drawQuad(x, y, x + w, y + h);

	glColor3f(color.r, color.g, color.b);
	drawQuad(x, y, x + w * progress, y + h);
	glKosSwapBuffers();
}
