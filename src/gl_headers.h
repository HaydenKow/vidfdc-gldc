#pragma once

/* All opengl geaders for pc/dc, builtin or library */
 #ifndef BUILD_LIBGL
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glkos.h>
#include <GL/glext.h>
#else
#include "gl.h"
#include "glu.h"
#include "glkos.h"
#include "glext.h"
#endif