#ifndef PCXLOADER_H
#define PCXLOADER_H

#include "gl_headers.h"

int loadtxr(const char *fn, GLuint *txr, unsigned int *w, unsigned int *h);

#endif
