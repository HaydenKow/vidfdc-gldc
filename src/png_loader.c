#include "gl_headers.h"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_FAILURE_STRINGS

#define STBI_NO_BMP
#define STBI_NO_PSD
#define STBI_NO_TGA
#define STBI_NO_GIF
#define STBI_NO_HDR
#define STBI_NO_PIC
#define STBI_NO_PNM

#include "stb_image.h"

int loadpng(const char *fn, GLuint *txr, unsigned *w, unsigned *h) {
	int channels;
	stbi_set_flip_vertically_on_load(0);
	char *data;
	data = stbi_load(fn, w, h, &channels, STBI_rgb_alpha);

	glGenTextures(1, txr);
	glBindTexture(GL_TEXTURE_2D, *txr);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, *w, *h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	stbi_image_free(data);

	return 0;
}
