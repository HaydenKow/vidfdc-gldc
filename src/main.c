
#include <kos.h>
#include "gl_headers.h"
#include <stdio.h>
#include <dc/pvr.h>

#include "asset_loader.h"

#include "dc_render.h"

#include "data_estructures.h"

#include "matrix.h"

extern uint8 romdisk[];

KOS_INIT_FLAGS(INIT_DEFAULT | INIT_MALLOCSTATS);
KOS_INIT_ROMDISK(romdisk);

pDemoAssetList demo_content;

int main(int argc, char *argv[]) {
  GLdcConfig config;
  glKosInitConfig(&config);

  glKosInitEx(&config);

	// Load assets
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, 640, 0, 480, -1, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	demo_content = init_AssetList();
	asset_loader(demo_content);

	// Run demo
	int ret = demo_render(demo_content);
	if(ret) {
		puts("Error while running demo.");
	}

	return ret;
}
