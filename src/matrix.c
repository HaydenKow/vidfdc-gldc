#include <string.h>
#include <math.h>

void setInfinitePerspectiveMat(float *m, float aspect, float fovDegrees, float const near) {
	float const focal = 1.f / tanf(fovDegrees / 360.f * M_PI);

	memset(m, 0, 16 * sizeof(float));
	m[0] = focal / aspect;
	m[5] = focal;
	m[10] = -1.f;
	m[11] = -2.f * near;
	m[14] = -1.f;
}
