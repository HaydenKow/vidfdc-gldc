// Data structures for the VIDF
#include <kos.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "data_estructures.h"

pmesh retrieveMesh(p3DModelList l, int pos)  // returns the nth mesh of the list of 3dmeshes
{
	int i = pos;
	p3DModelList curr = l;

	for(; i != 0 && curr != NULL; --i) {
		curr = curr->next;
	}

	// printf("retrieveMesh : pos : %d, i : %d \n", pos, i);
	return curr->mesh;
}

int retrieveTextureHandle(pTextureList l, int pos) {

	int i = 0;
	pTextureList curr = l;

	for(; i != pos && curr != NULL; i++) {
		curr = curr->next;
	}

	return curr->texture_handle;
}

char *retrieveTextureName(pTextureList l, int pos) {

	int i = 0;
	pTextureList curr = l;

	for(; i != pos && curr != NULL; i++) {
		curr = curr->next;
	}

	return (char *)curr->name;
}

int getMeshPos(p3DModelList l, char *name) {
	int ret = 0;
	int strcmp_res = 99;
	p3DModelList iter = l;
	strcmp_res = strcmp(name, iter->name);  // compare the names
	if(strcmp_res == 0) {
		// printf("Modelo encontrado : %s \t pos: %d\n", name, ret);
		return 0;
	}
	iter = iter->next;  // obviously the 1st position is not it.

	for(; iter != NULL && strcmp_res != 0; iter = iter->next) {
		strcmp_res = strcmp(name, iter->name);  // compare the names
		ret++;
		// iter = iter->next ;
	}

	if(strcmp_res == 0) {
		// printf("Modelo encontrado : %s \t pos: %d\n", name, ret);
	} else {
		// printf("nope \n");
	}

	return ret;
}

int getTexturePos(pTextureList l, char *name) {
	int ret = 0;
	pTextureList iter = l;

	while((strcmp(name, iter->name) != 0) && (iter != NULL)) {
		ret++;
		// printf("ret %d \t name %s", ret, iter->name);
		iter = iter->next;
	}
	// printf("Texture %s, pos %d \n \n", name, ret);
	return ret;
}

pSceneDescription newSceneDescription() {
	pSceneDescription ret = (pSceneDescription)malloc(sizeof(SceneDescription));

	ret->name = NULL;
	ret->mesh_name = NULL;
	ret->texture_name = NULL;

	ret->speed.x = 0.0f;
	ret->speed.y = 0.0f;
	ret->speed.z = 0.0f;

	ret->next = NULL;

	ret->render_effects = 0;
	ret->flags = 0;
	ret->angle = 0.0;
	ret->transforms = NULL;

	return ret;
}

pTransformList addTransform(pTransformList l, int transformType, vec3 v, float scalar) {
	// adds a transform to the linked list. It will ALLWAYS return the linked list head.
	pTransformList ret = (pTransformList)malloc(sizeof(TransformList));

	ret->transformType = transformType;
	ret->next = NULL;

	(ret->transformData).x = v.x;
	(ret->transformData).y = v.y;
	(ret->transformData).z = v.z;

	if(transformType == ROTATION) {
		ret->scalar_val = scalar;
	} else {
		ret->scalar_val = 0;
	}

	pTransformList iter = l;

	if(l == NULL) {
		return ret;
	}

	while(iter->next != NULL) {
		iter = iter->next;
	}

	iter->next = ret;

	return l;
}

void SetSceneDescriptorFlag(pSceneDescription sd, const int f) { sd->flags = sd->flags | f; }

void setSceneDescriptionRender(pSceneDescription a, int mode) { a->render_effects = (a->render_effects | mode); }

void setSceneDescriptionAngle(pSceneDescription a, float angle) { a->angle = angle; }
pSceneDescription addToSceneDescriptionList(pSceneDescription head, pSceneDescription item)  // adds a SceneDescriptor to the list. allways returns the head
{
	if(head == NULL) {
		return item;
	} else {
		pSceneDescription curr = head;

		while(curr->next != NULL) {
			curr = curr->next;
		}
		curr->next = item;
		return head;
	}
}

void addToSceneDescriptionName(pSceneDescription sd, char const *const name) { sd->name = strdup(name); }

void addToSceneDescriptionMesh(pSceneDescription sd, char const *const mesh_name) { sd->mesh_name = strdup(mesh_name); }
void addToSceneDescriptionTextureName(pSceneDescription sd, char const *const texture_name) { sd->texture_name = strdup(texture_name); }

//TODO :: REMOVE WHEN XML FORMAT IS UPDATED

void addToSceneDescriptor2DoverlayStart(pSceneDescription sd, uint64 a) { sd->start = a; }

void addToSceneDescriptor2DoverlayEnd(pSceneDescription sd, uint64 a) { sd->end = a; }

void addToSceneDescriptor2DoverlayFade_In(pSceneDescription sd, uint64 a) { sd->fade_in = a; }

void addToSceneDescriptor2DoverlayFade_Out(pSceneDescription sd, uint64 a) { sd->fade_out = a; }

// #############################################################


void addToSceneDescriptorStartTime(pSceneDescription sd, uint64 a) { sd->start = a; } 

void addToSceneDescriptorEndTime(pSceneDescription sd, uint64 a) { sd->end = a; }

void addToSceneDescriptorFade_In(pSceneDescription sd, uint64 a) { sd->fade_in = a; }

void addToSceneDescriptorFade_Out(pSceneDescription sd, uint64 a) { sd->fade_out = a; }



pCameraList newCamera() {
	pCameraList ret;
	ret = (pCameraList)malloc(sizeof(CameraList));

	ret->start_pos.x = ret->start_pos.y = ret->start_pos.z = 0.0f;
	ret->end_pos.x = ret->end_pos.y = ret->end_pos.z = 0.0f;

	ret->start_looking_direction.x = ret->start_looking_direction.y = ret->start_looking_direction.z = 0.0f;
	ret->end_looking_direction.x = ret->end_looking_direction.y = ret->end_looking_direction.z = 0.0f;

	ret->start_rotation.x = ret->start_rotation.y = ret->start_rotation.z = 0.0f;
	ret->end_rotation.x = ret->end_rotation.y = ret->end_rotation.z = 0.0f;

	ret->time_start = 0;
	ret->time_end = 0;

	ret->id = 0;
	ret->next = NULL;

	ret->start_fov = 90.0f;
	ret->end_fov = 90.0f;

	ret->angle = 0.0f;

	ret->flags = 0;

	return ret;
}

void setCameraAngle(pCameraList l, float a) { l->angle = a; }
void setCameraPosStart(pCameraList l, vec3 start_pos) {

	l->start_pos.x = start_pos.x;
	l->start_pos.y = start_pos.y;
	l->start_pos.z = start_pos.z;
}

void setCameraPosEnd(pCameraList l, vec3 end_pos) {
	l->end_pos.x = end_pos.x;
	l->end_pos.y = end_pos.y;
	l->end_pos.z = end_pos.z;
}

void setCameraLookingDirectionStart(pCameraList l, vec3 start) {
	l->start_looking_direction.x = start.x;
	l->start_looking_direction.y = start.y;
	l->start_looking_direction.z = start.z;
}

void setCameraLookingDirectionEnd(pCameraList l, vec3 end) {
	l->end_looking_direction.x = end.x;
	l->end_looking_direction.y = end.y;
	l->end_looking_direction.z = end.z;
}

void setCameraRotationStart(pCameraList l, vec3 start) {
	l->start_rotation.x = start.x;
	l->start_rotation.y = start.y;
	l->start_rotation.z = start.z;
}

void setCameraRotationEnd(pCameraList l, vec3 end) {
	l->end_rotation.x = end.x;
	l->end_rotation.y = end.y;
	l->end_rotation.z = end.z;
}

void setCameraStartTime(pCameraList l, long int a) { l->time_start = a; }

void setCameraEndTime(pCameraList l, long int a) { l->time_end = a; }

void setCameraId(pCameraList l, int a) { l->id = a; }

void setCameraFovStart(pCameraList l, float f) { l->start_fov = f; }

void setCameraFovEnd(pCameraList l, float f) { l->end_fov = f; }

pCameraList addToCameraList(pCameraList head, pCameraList item) {
	int local_id = 1;

	if(head == NULL) {
		setCameraId(item, local_id);
		return item;
	} else {
		pCameraList curr = head;

		while(curr->next != NULL) {
			local_id = local_id + 1;
			curr = curr->next;
		}
		setCameraId(item, local_id);
		curr->next = item;
		return head;
	}
}

void setCameraFlag(pCameraList c, const int f) { c->flags = c->flags | f; }

int isCameraInTime(pCameraList l, const unsigned long curr_time) {
	if((l->time_start <= curr_time) && (curr_time < l->time_end)) {
		return 1;  // the camera is inside the current time-frame
	} else {
		return 0;  // the camera is outside the current time-frame.
	}
}

void SceneDescriptorPosUpdate(pSceneDescription sd, unsigned long curr_time)  // returns the number of descriptors updated
{
	// pos = old_pos * (time*speed)
	while(sd->next != NULL) {
	}
}

void CameraListPosUpdate(pCameraList cl, unsigned long curr_time) {
	pCameraList iter = cl;
	while(iter != NULL) {
		if(isCameraInTime(iter, curr_time)) {
			if(iter->flags & START_POS)  // if there is START_POS assume END_POS
			{
				if((iter->flags & VEL_SET) == 0) {
					// printf("set vel\n");
					// velocity should be set
					iter->velocity = getSpeedVec3(iter->start_pos, iter->end_pos, iter->time_start, iter->time_end);
					iter->velocity_lookat = getSpeedVec3(iter->start_looking_direction, iter->end_looking_direction, iter->time_start, iter->time_end);
					setCameraFlag(iter, VEL_SET);
				}

				// printf("\nSPEED : x y z %f %f %f \n", (iter->velocity).x, (iter->velocity).y, (iter->velocity).z);
				iter->start_pos.x += ((iter->velocity).x * curr_time * 0.001f);
				iter->start_pos.y += ((iter->velocity).y * curr_time * 0.001f);
				iter->start_pos.z += ((iter->velocity).z * curr_time * 0.001f);

				//LOOK AT destination update

				iter->start_looking_direction.x += ((iter->velocity).x * curr_time * 0.001f);
				iter->start_looking_direction.y += ((iter->velocity).y * curr_time * 0.001f);
				iter->start_looking_direction.z += ((iter->velocity).z * curr_time * 0.001f);


			}
		} else {

			// dont touch it
		}

		iter = iter->next;
	}
}

vec3 getSpeedVec3(vec3 start_pos, vec3 end_pos, unsigned long start_time, unsigned long end_time) {

	// speed = Delta_dist / Delta_t
	unsigned long diff = end_time - start_time;
	vec3 ret;
	vec3 tmp;

	tmp.x = 0.0f;
	tmp.y = 0.0f;
	tmp.z = 0.0f;

	tmp.x = end_pos.x - start_pos.x;
	tmp.y = end_pos.y - start_pos.y;
	tmp.z = end_pos.z - start_pos.z;

	ret.x = tmp.x / diff;
	ret.y = tmp.y / diff;
	ret.z = tmp.z / diff;

	return ret;
}

pTexFontList newTexfont(char *name) {
	pTexFontList ret;
	ret = (pTexFontList)malloc(sizeof(TexFontList));
	ret->name = strdup(name);
	ret->texture_handle = 0;
	ret->next = NULL;

	return ret;
}

inline int retrieveTextureWidth(pTextureList a, int pos) {
	int i = 0;
	pTextureList curr = a;
	for(; i != pos; i++) {
		curr = curr->next;
	}
	return curr->width;
}

inline int retrieveTextureHeight(pTextureList a, int pos) {
	int i = 0;
	pTextureList curr = a;
	for(; i != pos; i++) {
		curr = curr->next;
	}
	return curr->height;
}

// adds a model to the 3DModel List. mainly to be used from the xml_parser.c to deal with precedural generated geometry

p3DModelList add_pmesh(p3DModelList l, pmesh m, char const *const name) {

	p3DModelList ret = (p3DModelList)malloc(sizeof(_3DModelList));

	ret->mesh = m;
	ret->name = strdup(name);
	ret->next = NULL;

	if(l == NULL) {
		printf("NULL @ add_pmesh");
		return ret;
	} else {
		p3DModelList curr = l;

		while(curr->next != NULL) {
			curr = curr->next;
		}
		curr->next = ret;

		return l;
	}
}

pStats initStats()
{
	pStats ret = (pStats) malloc (sizeof(Stats)) ;
	 
	ret->totalLoadingTime = 0;
	ret->totalMeshesPerFrame = 0;
	ret->totalVerticesPerFrame = 0;
	ret->totalLightPerFrame = 0;
	ret->averageRenderTime  = 0;

	return ret ;
}

void resetStatsPerFrame(pStats s)
{
	s->totalMeshesPerFrame = 0;
	s->totalVerticesPerFrame = 0;
	s->totalLightPerFrame = 0;
}


void addMeshStat(pStats s, pmesh p)
{
	s->totalMeshesPerFrame = s->totalMeshesPerFrame + 1 ;
	s->totalVerticesPerFrame = s->totalVerticesPerFrame + (unsigned int) p->vertex_count ; 
}