/*****************************************************************
 * outline.c
 *
 * Copyright 1999, Clark Cooper
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the license contained in the
 * COPYING file that comes with the expat distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Read an XML document from standard input and print an element
 * outline on standard output.
 * Must be used with Expat compiled for UTF-8 output.
 */

#include <kos.h>
#include <stdio.h>
#include <expat/expat.h>
#include "data_estructures.h"

#if defined(__amigaos__) && defined(__USE_INLINE__)
#include <proto/expat.h>
#endif

#ifdef XML_LARGE_SIZE
#if defined(XML_USE_MSC_EXTENSIONS) && _MSC_VER < 1400
#define XML_FMT_INT_MOD "I64"
#else
#define XML_FMT_INT_MOD "ll"
#endif
#else
#define XML_FMT_INT_MOD "l"
#endif

#define BUFFSIZE 8192

#define R_CHAR 1

char Buff[BUFFSIZE];

int Depth;

unsigned int char_pos;

pTexFontList f;

static void XMLCALL start(void *data, const char *el, const char **attr) {
	int i;

	if(strcmp(el, "char") == 0) {

		char_pos = atoi(attr[1]);
		if(char_pos < 255) {

			f->charCoord[char_pos].x = atof(attr[3]);
			f->charCoord[char_pos].y = atof(attr[5]);
			f->charCoord[char_pos].width = atof(attr[7]);
			f->charCoord[char_pos].height = atof(attr[9]);

		} else {
			// DROP
		}
	}

	for(i = 0; attr[i]; i += 2) {
		// printf(" %s='%s'", attr[i], attr[i + 1]);
	}

	// printf("\n");
	Depth++;
}

static void XMLCALL end(void *data, const char *el) { Depth--; }

// int load_font(char* filename, pDemoAssetList assets, char* name)
int load_font(char *filename, pTexFontList ret, char *name) {

	f = ret;  // f must point to our font on assets, assignment kept to avoid any major rewite

	FILE *in_Stream;
	in_Stream = fopen(filename, "r");

	XML_Parser p = XML_ParserCreate(NULL);
	if(!p) {
		fprintf(stderr, "Couldn't allocate memory for parser\n");
		return -1;
	}

	XML_SetElementHandler(p, start, end);

	char buff;
	int done = 0;

	for(fread(&buff, sizeof(char), 1, in_Stream); !feof(in_Stream); fread(&buff, sizeof(char), 1, in_Stream))
		if(!XML_Parse(p, &buff, sizeof(char), done)) {
			fprintf(stderr, "fnt....Parse error at line %lu:\n%s\n", XML_GetCurrentLineNumber(p), XML_ErrorString(XML_GetErrorCode(p)));
			return -1;
		}

	// no need to return ret, we are accessing its values directly trough its pointer
	return 0;
}
